package io4.domain;

import static org.hamcrest.collection.IsEmptyCollection.empty;
import static org.hamcrest.collection.IsIterableContainingInOrder.contains;
import static org.junit.Assert.assertThat;

import org.junit.Test;

public class StoreTest {
	@Test
	public void testNewInstance() {
		Store store = new Store();
		assertThat(store.articles(), empty());
	}
	
	@Test
	public void testAdd() {
		Article article1 = new Article("article1", 100, 1);
		Article article2 = new Article("article2", 200, 2);
		Store store = new Store();
		store.add(article1);
		store.add(article2);
		assertThat(store.articles(), contains(article1, article2));
	}
}
