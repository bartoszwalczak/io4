package io4.domain;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class ArticleTest {
	@Test
	public void testNewInstance() {
		Article article = new Article("article1", 1, 100);
		assertEquals("article1", article.name());
		assertEquals(1, article.count());
		assertEquals(100, article.price());
	}
	
	@Test
	public void testUpdate() {
		Article article = new Article("article1", 1, 100);
		article.update("article2", 2, 200);
		assertEquals("article2", article.name());
		assertEquals(2, article.count());
		assertEquals(200, article.price());
	}
}
