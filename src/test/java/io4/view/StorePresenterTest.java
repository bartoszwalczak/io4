package io4.view;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import io4.domain.Article;
import io4.domain.Store;

import org.junit.Test;

public class StorePresenterTest {
	@Test
	public void testOpen() {
		Store store = new Store();
		store.add(new Article("article1", 100, 1));
		store.add(new Article("article2", 200, 2));
		StoreView view = mock(StoreView.class);
		StorePresenter presenter = new StorePresenter(store, view, null);
		presenter.open();
		verify(view).addItem("article1: 100 at $1");
		verify(view).addItem("article2: 200 at $2");
		verify(view).open(presenter);
	}
	
	@Test
	public void testEditArticle() {
		Store store = new Store();
		Article article1 = new Article("article1", 100, 1);
		Article article2 = new Article("article2", 200, 2);
		store.add(article1);
		store.add(article2);
		StoreView view = mock(StoreView.class);
		ViewFactory factory = mock(ViewFactory.class);
		StorePresenter presenter = new StorePresenter(store, view, factory);
		presenter.open();
		presenter.editArticle(1);
		verify(factory).openEditArticleView(article2);
	}
	
	@Test
	public void testAddArticle() {
		Store store = new Store();
		StoreView view = mock(StoreView.class);
		ViewFactory factory = mock(ViewFactory.class);
		StorePresenter presenter = new StorePresenter(store, view, factory);
		presenter.open();
		presenter.addArticle();
		verify(factory).openAddArticleView(store);
	}
}
