package io4.domain;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Store {
	public interface Observer {
		void notifyAdd(Article article);
	}
	
	private List<Observer> observers = new ArrayList<Observer>();
	
	public void addObserver(Observer observer) { observers.add(observer); }
	public void removeObserver(Observer observer) { observers.remove(observer); }
	
	private List<Article> articles = new ArrayList<Article>();
	
	public List<Article> articles() { return Collections.unmodifiableList(articles); }
	
	public void add(Article article) {
		articles.add(article);
		for (Observer observer : observers) observer.notifyAdd(article);
	}
	
	public void increasePrices(int amount) {
		// very complex logic...
		for (Article article : articles())
			article.update(article.name(), article.count(),
					article.price() + amount);
	}
}
