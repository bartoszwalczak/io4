package io4.view;

import io4.domain.Article;
import io4.domain.Store;

public interface ViewFactory {
	void openEditArticleView(Article article);
	void openAddArticleView(Store store);
	void openStoreView(Store store);
}
