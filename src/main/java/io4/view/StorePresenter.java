package io4.view;

import io4.domain.Article;
import io4.domain.Store;

public class StorePresenter {
	private final Store store;
	private final StoreView view;
	private final ViewFactory factory;
	
	public StorePresenter(Store store, StoreView view, ViewFactory factory) {
		this.store = store;
		this.view = view;
		this.factory = factory;
	}
	
	private static String articleDescription(Article article) {
		return article.name() + ": " + article.count() + " at $" + article.price();
	}
	
	public void open() {
		for (Article article : store.articles()) {
			view.addItem(articleDescription(article));
			article.addObserver(articleObserver);
		}
		store.addObserver(storeObserver);
		view.open(this);
	}
	
	public void editArticle(int index) {
		factory.openEditArticleView(store.articles().get(index));
	}
	public void addArticle() {
		factory.openAddArticleView(store);
	}
	public void close() {
		view.close();
		store.removeObserver(storeObserver);
		for (Article article : store.articles()) article.removeObserver(articleObserver);
	}
	
	private Article.Observer articleObserver = new Article.Observer() {
		public void notifyUpdate(Article article) {
			int index = store.articles().indexOf(article);
			view.setItem(index, articleDescription(article));
		}
	};
	private Store.Observer storeObserver = new Store.Observer() {
		public void notifyAdd(Article article) {
			view.addItem(articleDescription(article));
			article.addObserver(articleObserver);
		}
	};
}
