package io4.view;

import io4.db.DbGateway;
import io4.domain.Article;
import io4.domain.Store;

public class ArticlePresenter {
	private interface ConfirmAction {
		void perform(String name, int count, int price);
	}
	
	private final ArticleView view;
	private ConfirmAction confirmAction;
	
	public ArticlePresenter(ArticleView view) { this.view = view; }
	
	public void openForEdit(final Article article, final DbGateway db) {
		confirmAction = new ConfirmAction() {
			public void perform(String name, int count, int price) {
				article.update(name, count, price);
				db.updateArticle(article);
			}
		};
		view.setArticleName(article.name());
		view.setArticleCount(String.valueOf(article.count()));
		view.setArticlePrice(String.valueOf(article.price()));
		view.open(this);
	}
	public void openForAdd(final Store store, final DbGateway db) {
		confirmAction = new ConfirmAction() {
			public void perform(String name, int count, int price) {
				Article article = new Article(name, count, price);
				store.add(article);
				db.addArticle(article);
			}
		};
		view.open(this);
	}
	
	public void confirm() {
		try {
			String name = view.getArticleName();
			int count = Integer.parseInt(view.getArticleCount());
			int price = Integer.parseInt(view.getArticlePrice());
			confirmAction.perform(name, count, price);
			view.close();
		}
		catch (NumberFormatException ex) {
			// report error somehow
		}
	}
	public void cancel() { view.close(); }
}
