package io4.view;

public interface StoreView {
	void addItem(String item);
	void setItem(int index, String item);
	
	void open(StorePresenter presenter);
	void close();
}
