package io4.view;

public interface ArticleView {
	String getArticleName();
	String getArticleCount();
	String getArticlePrice();
	void setArticleName(String name);
	void setArticleCount(String count);
	void setArticlePrice(String price);
	
	void open(ArticlePresenter presenter);
	void close();
}
