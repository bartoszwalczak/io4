package io4.gui;

import io4.db.DbGateway;
import io4.domain.Store;

import javax.swing.SwingUtilities;

public class GUIFacade {
	public static void start(final Store store, final DbGateway database) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				WindowFactory factory = new WindowFactory(database);
				factory.openStoreView(store);
			}
		});
	}
}
