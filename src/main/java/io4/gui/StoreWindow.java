package io4.gui;

import io4.view.StorePresenter;
import io4.view.StoreView;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JList;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class StoreWindow extends JDialog implements StoreView {
	private final DefaultListModel<String> model = new DefaultListModel<String>();
	
	public void open(final StorePresenter presenter) {
		setBounds(100, 100, 450, 300);
		
		final JList<String> list = new JList<String>(model);
		getContentPane().add(list, BorderLayout.CENTER);
		
		JPanel panel = new JPanel();
		getContentPane().add(panel, BorderLayout.SOUTH);
		
		JButton btnEditArticle = new JButton("Edit article");
		btnEditArticle.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int index = list.getSelectedIndex();
				if (index != -1) presenter.editArticle(index);
			}
		});
		panel.add(btnEditArticle);
		
		JButton btnAddArticle = new JButton("Add article");
		btnAddArticle.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) { presenter.addArticle(); }
		});
		panel.add(btnAddArticle);
		
		JButton btnClose = new JButton("Close");
		btnClose.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) { presenter.close(); }
		});
		panel.add(btnClose);
		
		setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		setVisible(true);
	}
	
	public void addItem(String item) { model.addElement(item); }
	public void setItem(int index, String item) { model.set(index, item); }
	
	public void close() {
		setVisible(false);
		dispose();
	}
}
