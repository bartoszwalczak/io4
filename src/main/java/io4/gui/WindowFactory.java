package io4.gui;

import io4.db.DbGateway;
import io4.domain.Article;
import io4.domain.Store;
import io4.view.ArticlePresenter;
import io4.view.StorePresenter;
import io4.view.ViewFactory;

public class WindowFactory implements ViewFactory {
	private final DbGateway database;
	
	public WindowFactory(DbGateway database) { this.database = database; }
	
	public void openEditArticleView(Article article) {
		ArticleWindow view = new ArticleWindow();
		ArticlePresenter presenter = new ArticlePresenter(view);
		presenter.openForEdit(article, database);
	}
	public void openAddArticleView(Store store) {
		ArticleWindow view = new ArticleWindow();
		ArticlePresenter presenter = new ArticlePresenter(view);
		presenter.openForAdd(store, database);
	}
	public void openStoreView(Store store) {
		StoreWindow view = new StoreWindow();
		StorePresenter presenter = new StorePresenter(store, view, this);
		presenter.open();
	}
}
