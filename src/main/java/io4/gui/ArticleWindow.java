package io4.gui;

import io4.view.ArticlePresenter;
import io4.view.ArticleView;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

@SuppressWarnings("serial")
public class ArticleWindow extends JDialog implements ArticleView {
	private final JFormattedTextField nameField = new JFormattedTextField("");
	private final JFormattedTextField countField = new JFormattedTextField("");
	private final JFormattedTextField priceField = new JFormattedTextField("");
	
	public void open(final ArticlePresenter presenter) {
		setBounds(150, 150, 350, 180);
		getContentPane().setLayout(new BorderLayout());
		
		JPanel contentPanel = new JPanel();
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPanel.setLayout(new BoxLayout(contentPanel, BoxLayout.Y_AXIS));
		contentPanel.add(nameField);
		contentPanel.add(countField);
		contentPanel.add(priceField);
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		
		JPanel buttonPane = new JPanel();
		buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
		getContentPane().add(buttonPane, BorderLayout.SOUTH);
		
		JButton okButton = new JButton("OK");
		okButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) { presenter.confirm(); }
		});
		buttonPane.add(okButton);
		getRootPane().setDefaultButton(okButton);
		
		JButton cancelButton = new JButton("Cancel");
		cancelButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) { presenter.cancel(); }
		});
		buttonPane.add(cancelButton);
		
		setVisible(true);
	}
	
	public String getArticleName() { return (String)nameField.getValue(); }
	public String getArticleCount() { return (String)countField.getValue(); }
	public String getArticlePrice() { return (String)priceField.getValue(); }
	public void setArticleName(String name) { nameField.setValue(name); }
	public void setArticleCount(String count) { countField.setValue(count); }
	public void setArticlePrice(String price) { priceField.setValue(price); }
	
	public void close() {
		setVisible(false);
		dispose();
	}
}
