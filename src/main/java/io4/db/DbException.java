package io4.db;

@SuppressWarnings("serial")
public class DbException extends RuntimeException {
	public DbException(Exception ex) { super(ex); }
}
