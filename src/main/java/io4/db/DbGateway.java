package io4.db;

import io4.domain.Article;
import io4.domain.Store;

public interface DbGateway {
	Store loadStore();
	void addArticle(Article article);
	void updateArticle(Article article);
}
