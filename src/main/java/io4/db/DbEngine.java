package io4.db;

public interface DbEngine {
	interface ArticleListBuilder {
		void add(int id, String name, int count, int price);
	}
	
	void readArticles(ArticleListBuilder builder);
	int insertArticle(String name, int count, int price);
	void updateArticle(int id, String name, int count, int price);
}
