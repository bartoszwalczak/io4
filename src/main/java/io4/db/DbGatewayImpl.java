package io4.db;

import io4.domain.Article;
import io4.domain.Store;

import java.util.HashMap;
import java.util.Map;

public class DbGatewayImpl implements DbGateway {
	private final DbEngine engine;
	private Map<Article, Integer> ids = null;
	
	public DbGatewayImpl(DbEngine engine) { this.engine = engine; }
	
	public Store loadStore() {
		final Store store = new Store();
		ids = new HashMap<Article, Integer>();
		engine.readArticles(new DbEngine.ArticleListBuilder() {
			public void add(int id, String name, int count, int price) {
				Article article = new Article(name, count, price);
				store.add(article);
				ids.put(article, id);
			}
		});
		return store;
	}
	public void addArticle(Article article) {
		int id = engine.insertArticle(article.name(), article.count(), article.price());
		ids.put(article, id);
	}
	public void updateArticle(Article article) {
		int id = ids.get(article);
		engine.updateArticle(id, article.name(), article.count(), article.price());
	}
}
