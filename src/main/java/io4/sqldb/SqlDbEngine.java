package io4.sqldb;

import io4.db.DbEngine;
import io4.db.DbException;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class SqlDbEngine implements DbEngine {
	private final String url;
	private final String user;
	private final String password;
	
	public SqlDbEngine(String url, String user, String password) {
		this.url = url;
		this.user = user;
		this.password = password;
	}
	
	private Connection getConnection() throws SQLException {
		return DriverManager.getConnection(url, user, password);
	}
	
	public void readArticles(ArticleListBuilder builder) {
		try (Connection connection = getConnection();
		     PreparedStatement sql = connection.prepareStatement(
					"SELECT * FROM io_articles");
		     ResultSet rs = sql.executeQuery())
		{
			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");
				int count = rs.getInt("count");
				int price = rs.getInt("price");
				builder.add(id, name, count, price);
			}
		}
		catch(SQLException ex) { throw new DbException(ex); }
	}
	
	public int insertArticle(String name, int count, int price) {
		try (Connection connection = getConnection();
		     PreparedStatement sql = connection.prepareStatement(
		    		 "INSERT INTO io_articles (name, count, price) " + 
		    		 "VALUES (?, ?, ?) RETURNING id"))
		{
			sql.setString(1, name);
			sql.setInt(2, count);
			sql.setInt(3, price);
			try (ResultSet rs = sql.executeQuery()) {
				rs.next();
				return rs.getInt("id");
			}
		}
		catch(SQLException ex) { throw new DbException(ex); }
	}
	
	public void updateArticle(int id, String name, int count, int price) {
		try (Connection connection = getConnection();
		     PreparedStatement sql = connection.prepareStatement(
					"UPDATE io_articles SET (name, count, price) = (?, ?, ?) " +
					"WHERE id = ?"))
		{
			sql.setString(1, name);
			sql.setInt(2, count);
			sql.setInt(3, price);
			sql.setInt(4, id);
			sql.executeUpdate();
		}
		catch(SQLException ex) { throw new DbException(ex); }
	}
}
