package io4;

import io4.db.DbEngine;
import io4.db.DbGateway;
import io4.db.DbGatewayImpl;
import io4.domain.Store;
import io4.gui.GUIFacade;
import io4.sqldb.SqlDbFactory;

public class Program {
	public static void main(String[] args) {
		DbEngine engine = new SqlDbFactory().createEngine();
		DbGateway gateway = new DbGatewayImpl(engine);
		Store store = gateway.loadStore();
		GUIFacade.start(store, gateway);
	}
}
